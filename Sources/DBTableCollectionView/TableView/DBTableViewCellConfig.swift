//
//  AdapterTableViewCellConfig.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

open class DBTableViewCellConfig {
    internal var type: AnyClass
    internal var cellIdentifier: String!
    internal weak var delegate: DBProtocol?
    internal var modelType: DBModel.Type!
    internal var registerType : RegisterCellType!
    internal var bundle : Bundle?
    internal var nibName: String?
    internal var horizontalCellConfiguration: DBCollectionViewCellConfig?
    
    public required init<T: DBModel>(type: DBTableViewCellParent<T>.Type, cellIdentifier: String, delegate: DBProtocol?, modelType: DBModel.Type, registerType : RegisterCellType = .none, bundle : Bundle? = nil, nibName: String?, horizontalCellConfiguration: DBCollectionViewCellConfig? = nil) {
        self.type = type
        self.cellIdentifier = cellIdentifier
        self.delegate = delegate
        self.modelType = modelType
        self.registerType = registerType
        self.bundle = bundle
        self.nibName = nibName
        self.horizontalCellConfiguration = horizontalCellConfiguration
    }
    
    open func canEditing() -> Bool {
        return false
    }
    
    open func canMove() -> Bool {
        return false
    }
    
    open func deleteButtonTitle() -> String {
        return "Delete"
    }
    
    open func editingStyle() -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    open func getHeight(tableView: UITableView, section: DBTableViewSection) -> CGFloat {
        return UITableView.automaticDimension
    }
    

    //func getCell
}
