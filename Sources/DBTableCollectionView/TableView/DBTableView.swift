//
//  AdapterTableView.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol DBTableViewAdapterDelegate: NSObjectProtocol {
    @objc optional func scrollViewDidScroll(_ scrollView: UIScrollView)
    @objc optional func moved(item: DBModel)
    @objc optional func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
}

public enum DBTableViewStyle : Int {
    case plain = 0
    case grouped = 1
    case insetGrouped = 2
}


open class DBTableView: UITableView{
    
   
    public weak var tableViewDelegate: DBTableViewAdapterDelegate?  {
        didSet {
            adapter.delegate = tableViewDelegate
        }
    }
    internal var adapter: DBTableViewAdapter! {
        didSet {
            self.dataSource = adapter
            self.delegate = adapter
            //adapter.delegate = tableViewDelegate
        }
    }
    private var emptyModel = DBEmptyModel()
    private var retryModel = DBRetryModel()
    
    var isGlobalEmptyCell = false
    var isGlobalRetryCell = false
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        configureTableView()
    }
    
    
    public init(frame: CGRect, style: DBTableViewStyle) {
        if (style == .insetGrouped) {
            if #available(iOS 13.0, *) {
                super.init(frame: frame, style: .insetGrouped)
            }else{
                super.init(frame: frame, style: .grouped)
            }
        }else{
            super.init(frame: frame, style: UITableView.Style(rawValue: style.rawValue)!)
        }
        
        configureTableView()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        configureTableView()
        //fatalError("init(coder:) has not been implemented")
    }
    
    public func removeTopSpace(space: CGFloat = .leastNormalMagnitude) {
        var frame = CGRect.zero
        frame.size.height = space
        let v = UIView(frame: frame)
        v.backgroundColor = UIColor.clear
        self.tableHeaderView = v
    }
    
    public func removeBottomSpace() {
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        let v = UIView(frame: frame)
        v.backgroundColor = UIColor.clear
        self.tableFooterView = v
    }
    
    public func setEmptyModel(name: String, image: UIImage?, height: CGFloat) {
        emptyModel = DBEmptyModel(name: name, image: image, height: height)
    }

    public func setRetryModel(name: String, image: UIImage?, height: CGFloat) {
        retryModel = DBRetryModel(name: name, image: image, height: height)
    }
   
    private func configureTableView() {
        self.tableFooterView = UIView()
        self.estimatedRowHeight = 350
        //self.rowHeight = UITableView.automaticDimension
        self.reloadData()
        adapter = DBTableViewAdapter()
        
    }
    
    public func getItems() -> [DBModel] {
        return self.adapter.sections.reduce(into: [DBModel]()) { (itemss, section) in
            itemss.append(contentsOf: section.getItems())
        }
    }
    
    public func getSections() -> [DBTableViewSection] {
        return self.adapter.sections
    }
    
   
    
    public func registerCell(cell: DBTableViewCellConfig){
        adapter.cells.append(cell)
        switch cell.registerType {
        case .registerClass:
            self.register(cell.type, forCellReuseIdentifier: cell.cellIdentifier)
        case .registerNib:
            self.register(UINib(nibName:cell.nibName!, bundle: cell.bundle), forCellReuseIdentifier: cell.cellIdentifier)
        default:
            print("")
        }
    }
   

    // MARK: - Sections
    public func add(section: DBTableViewSection, startPosition: Int? = nil, animation: RowAnimation = .automatic) {
        let startPosition = startPosition ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition, animation: animation)
    }//Test Passed
    
    public func add(section: DBTableViewSection, after s: DBTableViewSection, animation: RowAnimation = .automatic) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition+1, animation: animation)
    }//Test Passed
    
    public func add(section: DBTableViewSection, before s: DBTableViewSection, animation: RowAnimation = .automatic) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition, animation: animation)
    }//Test Passed
    
    public func addAll(sections: [DBTableViewSection], startPosition: Int? = nil, animation: RowAnimation = .automatic) {
        let startPosition = startPosition ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition, animation: animation)
    }
    
    public func addAll(sections: [DBTableViewSection], after s: DBTableViewSection, animation: RowAnimation = .automatic) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition+1, animation: animation)
    }
    
    public func addAll(sections: [DBTableViewSection], before s: DBTableViewSection, animation: RowAnimation = .automatic) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition, animation: animation)
    }
    
    public func removeAll(from section: DBTableViewSection, animation: RowAnimation = .automatic) {
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        section.items.removeAll()
        animation == .none ? reloadData() : self.reloadSections(IndexSet(integer: sectionIndex), with: animation)
    }
    
    public func remove(section: DBTableViewSection, animation: RowAnimation = .automatic) {
        guard let sectionIndex = self.adapter.sections.remove(object: section) else {
            return
        }
        animation == .none ? reloadData() : self.deleteSections(IndexSet(integer: sectionIndex), with: animation)
    }
    
    public func removeAll() {
        self.adapter.sections.forEach{$0.items.removeAll()}
        self.adapter.sections.removeAll()
        self.reloadData()
    }
    
    // MARK:- Items
    public func add(item: DBModel, to section: DBTableViewSection, startPosition: Int? = nil, animation: RowAnimation = .automatic) {
        let startPosition = startPosition ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition, animation: animation)
    }
    
    public func add(item: DBModel, to section: DBTableViewSection, after i: DBModel, animation: RowAnimation = .automatic) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition+1, animation: animation)
    }
    
    public func add(item: DBModel, to section: DBTableViewSection, before i: DBModel, animation: RowAnimation = .automatic) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition, animation: animation)
    }
    
    public func addAll(items: [DBModel], to section: DBTableViewSection, startPosition: Int? = nil, animation: RowAnimation = .automatic) {
        let startPosition = startPosition ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition, animation: animation)
    }
    
    public func addAll(items: [DBModel], to section: DBTableViewSection, after i: DBModel, animation: RowAnimation = .automatic) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition+1, animation: animation)
    }
    
    public func addAll(items: [DBModel], to section: DBTableViewSection, before i: DBModel, animation: RowAnimation = .automatic) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition, animation: animation)
    }
    
    public func update(item: DBModel, animation: RowAnimation = .automatic) {
//        guard let sectionAndIndex =  else {
//            return
//        }
        self.adapter.findSections(by: item).forEach { (section: DBTableViewSection, index: Int) in
            if (section.horizontalSection != nil) {
                guard let itemIndex = section.horizontalSection!.items.index(object: item) else {
                    return
                }
                section.horizontalSection.items.remove(at: itemIndex)
                section.horizontalSection.items.insert(item, at: itemIndex)
                animation == .none ? reloadData() : self.reloadSections(IndexSet(integer: index), with: animation)
            }else{
                guard let itemIndex = section.items.index(object: item) else {
                    return
                }
                section.items.remove(at: itemIndex)
                section.items.insert(item, at: itemIndex)
                animation == .none ? reloadData() : self.reloadRows(at: [IndexPath(row: itemIndex, section: index)], with: animation)
            }
        }
        
        
    }
    
    public func update(section: DBTableViewSection, animation: RowAnimation = .automatic) {
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        self.reloadSections(IndexSet(integer: sectionIndex), with: animation)
    }
    
    public func replace(previousItem: DBModel, with item: DBModel, animation: RowAnimation = .automatic) {
        guard let sectionAndIndex = self.adapter.findSection(by: previousItem) else {
            return
        }
        guard let itemIndex = sectionAndIndex.section.items.index(object: previousItem) else {
            return
        }
        sectionAndIndex.section.items.remove(at: itemIndex)
        sectionAndIndex.section.items.insert(item, at: itemIndex)
        animation == .none ? reloadData() : self.reloadRows(at: [IndexPath(row: itemIndex, section: sectionAndIndex.index)], with: animation)
    }
    
    public func replaceAll(items: [DBModel], in section: DBTableViewSection) {
        if (!getSections().contains(section)) {
            section.items = items
            self.add(section: section, animation: .none)
        }else{
            self.beginUpdates()
            removeAll(from: section)
            addAll(items: items, to: section)
            self.endUpdates()
        }
    }
    
    public func isContains(_ t: DBModel.Type) -> Bool {
        return getItems().contains{type(of: $0) == t}
    }
    
    public func isContains(_ item: DBModel) -> Bool {
        return getItems().contains{$0 == item}
    }
    
    public func remove(item: DBModel, animation: RowAnimation = .automatic) {
        guard let sectionAndIndex = self.adapter.findSection(by: item) else {
            return
        }
        guard let itemIndex = sectionAndIndex.section.items.removeById(object: item) else {
            return
        }
        animation == .none ? reloadData() : self.deleteRows(at: [IndexPath(row: itemIndex, section: sectionAndIndex.index)], with: animation)
    }
    
    public func showEmptyItem() {
        isGlobalEmptyCell = true
        self.adapter.sections.removeAll()
        //self.reloadData()
        self.isScrollEnabled = false
        let s = DBTableViewSection(headerTitle: "", items: [emptyModel])
        //s.items.append(emptyModel)
        self.adapter.sections.insert(contentsOf: [s], at: 0)
        self.reloadData()
    }
    
    public func removeEmptyItem() {
        isGlobalEmptyCell = false
        self.adapter.sections.removeAll()
        self.isScrollEnabled = true
    }
    
    public func showEmptyItem(in section: DBTableViewSection) {
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        section.items.removeAll()
        section.items.append(section.emptyModel)
        self.reloadData()
        //self.reloadSections(IndexSet(integer: sectionIndex), with: .automatic)
    }
    
    public func showRetryItem() {
        isGlobalRetryCell = true
        self.adapter.sections.removeAll()
        self.reloadData()
        self.isScrollEnabled = false
        let s = DBTableViewSection()
        s.items.append(retryModel)
        self.adapter.sections.insert(contentsOf: [s], at: 0)
        self.reloadData()
    }
    
    public func removeRetryItem() {
        isGlobalRetryCell = false
        self.adapter.sections.removeAll()
        self.isScrollEnabled = true
    }
    
    public func showRetryItem(in section: DBTableViewSection) {
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        section.items.removeAll()
        section.items.append(retryModel)
        self.reloadSections(IndexSet(integer: sectionIndex), with: .automatic)
    }
    
    
    
    //MARK:- Helpers
    
    private func addSections(sections: [DBTableViewSection], startPosition: Int, animation: RowAnimation = .automatic) {
        if (sections.isEmpty) {
            return
        }
        
        if (isGlobalRetryCell || isGlobalEmptyCell) {
            if isGlobalEmptyCell {self.removeEmptyItem()}
            if isGlobalRetryCell {self.removeRetryItem()}
            self.reloadData()
        }
        
        var startPosition = startPosition
        if (startPosition < 0) {
            startPosition = 0
        }
        if (startPosition > self.adapter.sections.count) {
            startPosition = self.adapter.sections.count
        }
    
        self.adapter.sections.insert(contentsOf: sections, at: startPosition)
        var indexSet = IndexSet()
        for i in startPosition...(startPosition + sections.count-1) {
            indexSet.insert(i)
        }
        if (self.numberOfSections == self.adapter.sections.count || animation == .none) {
            reloadData()
        }else{
            self.beginUpdates()
            self.insertSections(indexSet, with: animation)
            self.endUpdates()
        }
        
    }
    
    public func findSection(by item: DBModel) -> DBTableViewSection? {
        return self.adapter.findSection(by: item)?.section
    }
    
    private func addItems(items: [DBModel], to section: DBTableViewSection, startPosition: Int, animation: RowAnimation = .automatic) {
        
        if (items.isEmpty) {
            return
        }
        
        if (isGlobalRetryCell || isGlobalEmptyCell) {
            if isGlobalEmptyCell {self.removeEmptyItem()}
            if isGlobalRetryCell {self.removeRetryItem()}
            self.reloadData()
        }
        
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            section.items = section.items + items
            add(section: section, animation: animation)
            return
        }
        
        checkHelpingItems(section: section, sectionIndex: sectionIndex)
        var startPosition = startPosition
        if (startPosition < 0) {
            startPosition = 0
        }
        if (startPosition > section.items.count) {
            startPosition = section.items.count
        }
        section.items.insert(contentsOf: items, at: startPosition)
        var indexPaths = [IndexPath]()
        for i in startPosition...(startPosition + items.count-1) {
            let indexPath = IndexPath(row: i, section: sectionIndex)
            indexPaths.append(indexPath)
        }
        if (animation == .none) {
            self.reloadData()
        }else{
            self.beginUpdates()
            self.insertRows(at: indexPaths, with: animation)
            self.endUpdates()
        }
    }
    
    private func checkHelpingItems(section: DBTableViewSection, sectionIndex: Int) {
        let retryIndex = section.items.remove(type: DBRetryModel.self)
        let emptyIndex = section.items.remove(type: DBEmptyModel.self)
        if (retryIndex != nil || emptyIndex != nil) {
            self.reloadSections(IndexSet(integer: sectionIndex), with: .automatic)
        }
    }
    
//    private func removeRetryItem() {
//        self.isScrollEnabled = true
//        if (!adapter.items.isEmpty) {
//            //remove(item: emptyModel)
//            let index = adapter.items.remove(object: retryModel)
//            if (index == nil) {
//                return
//            }
//            let indexPath = IndexPath(row: index!, section: 0)
//            self.deleteRows(at: [indexPath], with: .automatic)
//        }
//    }
    
    //    func removeEmptyItem() {
    //        if (adapter.items.last is EmptyAdapterModel) {
    //            remove(items.size - 1)
    //        }
    //    }
    
}
