//
//  TableViewAdapter.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

//protocol TableViewAdapterDelegate: class {
//    func scrollViewDidScroll(_ scrollView: UIScrollView)
//}

internal class DBTableViewAdapter: DBAdapterParent, UITableViewDataSource, UITableViewDelegate {
    // typealias M = T
    internal var cells = [DBTableViewCellConfig]()
    
    //internal var items = [DBModel]()
   
    
    internal weak var delegate: DBTableViewAdapterDelegate?
    
    internal var sections = [DBTableViewSection]()
    
    internal func findSection(by item: DBModel) -> (section: DBTableViewSection, index: Int)? {
        if (sections.count == 0) {
            return nil
        }
        var i = sections.firstIndex { (section) -> Bool in
            return section.items.index(object: item) != nil
        }
        if (i == nil) {
            i = sections.firstIndex { (section) -> Bool in
                return section.horizontalSection?.items.index(object: item) != nil
            }
        }
        if (i != nil) {
            return (sections[i!], i!)
        }
        return nil
    }
    
    internal func findSections(by item: DBModel) -> [(section: DBTableViewSection, index: Int)] {
        if (sections.count == 0) {
            return [(section: DBTableViewSection, index: Int)]()
        }
        var indexes = [Int]()
        for (index, element) in sections.enumerated() {
            if element.items.index(object: item) != nil {
                indexes.append(index)
            }
        }
        if (indexes.isEmpty) {
            for (index, element) in sections.enumerated() {
                if element.horizontalSection?.items.index(object: item) != nil {
                    indexes.append(index)
                }
            }
        }
        if (!indexes.isEmpty) {
            var a = [(section: DBTableViewSection, index: Int)]()
            indexes.forEach{a.append((sections[$0], $0))}
            return a
        }
        return [(section: DBTableViewSection, index: Int)]()
    }
    
    internal func getItem(indexPath: IndexPath) -> DBModel {
        let section = self.sections[indexPath.section]
        let item = section.items[indexPath.row]
        return item
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll!(scrollView)
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = getItem(indexPath: indexPath)
        let config = getCellForIndexPath(item: item)
        let cell = tableView.dequeueReusableCell(withIdentifier: config.cellIdentifier, for: indexPath) as! DBTableViewCellParent<DBModel>
        cell.registerHorizontalCell(configuration: config.horizontalCellConfiguration)
        cell.delegate = config.delegate
        cell.section = self.sections[indexPath.section]
        cell.configureCell(item: item)
        
        return cell
    }
    
    
   
    internal func getCellForIndexPath(item: DBModel) -> DBTableViewCellConfig {
        var cell: DBTableViewCellConfig?
        cells.forEach { (it) in
            if it.modelType == type(of: item) {
                cell = it
            }
        }
        if (cell == nil) {
            fatalError("There is no registered cell for \(item.self)")
        }
        return cell!
    }
    
  
    
//    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.updateConstraints()
//    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = getItem(indexPath: indexPath)
        if let value = item.height {
            return value
        }else{
            return getCellForIndexPath(item: getItem(indexPath: indexPath)).getHeight(tableView: tableView, section: self.sections[indexPath.section])
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        (tableView.cellForRow(at: indexPath) as! DBTableViewCellParent).cellDidSelect(item: getItem(indexPath: indexPath))
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section].headerTitle
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return self.sections[section].footerTitle
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath) as! DBTableViewCellParent).accessoryButtonTapped(item: getItem(indexPath: indexPath))
    }
   
    
    //MARK:- Editing
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        let item = getItem(indexPath: indexPath)
        if let value = item.canMove {
            return value
        }else{
            return getCellForIndexPath(item: item).canMove()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?{
        let item = getItem(indexPath: indexPath)
        if let value = item.deleteButtonTitle {
            return value
        }else{
            return getCellForIndexPath(item: item).deleteButtonTitle()
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            (tableView.cellForRow(at: indexPath) as! DBTableViewCellParent).deleteButtonTapped(item: getItem(indexPath: indexPath))
            
        }
        if editingStyle == .insert {
            (tableView.cellForRow(at: indexPath) as! DBTableViewCellParent).insertButtonTapped(item: getItem(indexPath: indexPath))
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let item = getItem(indexPath: indexPath)
        if let value = item.canEditing {
            return value
        }else{
            return getCellForIndexPath(item: item).canEditing()
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        let item = getItem(indexPath: indexPath)
        if let value = item.editingStyle {
            return value
        }else{
            return getCellForIndexPath(item: item).editingStyle()
        }
    }
    
//    internal func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
//        return false
//    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }

        header.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section].headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section].footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sections[section].headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return sections[section].footerHeight
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceSection = sections[sourceIndexPath.section]
        let destinationSection = sections[destinationIndexPath.section]
        
        let sourceItem = sourceSection.items[sourceIndexPath.row]
       // let destinationItem = destinationSection.items[destinationIndexPath.row]
        
        _ = sourceSection.items.remove(object: sourceItem)
        destinationSection.items.insert(sourceItem, at: destinationIndexPath.row)
        
    }
}
