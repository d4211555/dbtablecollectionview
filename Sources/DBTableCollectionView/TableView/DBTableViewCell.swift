//
//  AdapterTableViewCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

fileprivate protocol DBTableViewCellDelegate: AnyObject {
    associatedtype T
    func configureCell(item: T)
    func isForViewType(item: T) -> Bool
    func cellDidSelect(item: T)
    func deleteButtonTapped(item: T)
    func insertButtonTapped(item: T)
}



open class DBTableViewCell<B: DBModel, D>: DBTableViewCellParent<DBModel> {
    
    open class func getConfiguration(delegate: D?, nibName: String?, bundle: Bundle? = nil, registerType: RegisterCellType, horizontalCellConfiguration: DBCollectionViewCellConfig? = nil) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: self.self, cellIdentifier: DBTableViewCell.getClassName(), delegate: (delegate as? DBProtocol), modelType: B.self, registerType: registerType, bundle: bundle, nibName: nibName,  horizontalCellConfiguration: horizontalCellConfiguration)
    }
    
    open class func getConfiguration<Config>(delegate: D?, nibName: String?, bundle: Bundle? = nil, registerType: RegisterCellType, horizontalCellConfiguration: DBCollectionViewCellConfig? = nil) -> Config where Config: DBTableViewCellConfig{
        
        return Config(type: self.self, cellIdentifier: DBTableViewCell.getClassName(), delegate: (delegate as? DBProtocol), modelType: B.self, registerType: registerType, bundle: bundle, nibName: nibName,  horizontalCellConfiguration: horizontalCellConfiguration)
    }
    
    open class func getConfiguration(delegate: D?, nibName: String?, bundle: Bundle? = nil, registerType: RegisterCellType) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: self.self, cellIdentifier: DBTableViewCell.getClassName(), delegate: (delegate as? DBProtocol), modelType: B.self, registerType: registerType, bundle: bundle, nibName: nibName,  horizontalCellConfiguration: nil)
    }
    
    open class func getConfiguration<Config: DBTableViewCellConfig>(delegate: D?, nibName: String?, bundle: Bundle? = nil, registerType: RegisterCellType) -> Config{
        return Config(type: self.self, cellIdentifier: DBTableViewCell.getClassName(), delegate: (delegate as? DBProtocol), modelType: B.self, registerType: registerType, bundle: bundle, nibName: nibName,  horizontalCellConfiguration: nil)
    }
    
    public func removeHeaderSeparator() {
        if #available(iOS 13.0, *) {
        }else{
            let width = subviews[0].frame.width
            for view in subviews where view != contentView {
                if view.frame.width == width {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    public func validateValue(value: Any?, forConstraint: NSLayoutConstraint, onNilValueConstant: CGFloat, onValidValueConstant: CGFloat) -> Bool {
        if (value == nil) {
            forConstraint.constant = onNilValueConstant
            return false
        }else{
            forConstraint.constant = onValidValueConstant
            return true
        }
    }
    
    public var item: B!
   
    
    override fileprivate func isForViewType(item: DBModel) -> Bool {
        return item is B
    }
    
    override internal func configureCell(item: DBModel) {
        self.item = (item as! B)
        configureCell(item: item as! B)
    }
    
    override internal func deleteButtonTapped(item: DBModel) {
        deleteButtonTapped(item: item as! B)
    }
    
    override internal func cellDidSelect(item: DBModel) {
        cellDidSelect(item: item as! B)
    }
    
    override internal func insertButtonTapped(item: DBModel) {
        insertButtonTapped(item: item as! B)
    }
    
    override internal func accessoryButtonTapped(item: DBModel) {
        accessoryButtonTapped(item: item as! B)
    }
    
    open func accessoryButtonTapped(item: B){
        
    }
    
    open func configureCell(item: B) {
        
    }
    
    open func deleteButtonTapped(item: B) {
        
    }
    
    open func insertButtonTapped(item: B) {
        
    }
    
    open func cellDidSelect(item: B) {
        
    }
    
    open func getDelegate() -> D? {
        return delegate as? D
    }
    
    public class func getClassName() -> String {
        return String(describing: type(of: self))
    }
    
//    public func legacyInsetGroup() {
//        self.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
//        contentView.translatesAutoresizingMaskIntoConstraints = false
//        contentView.layoutAttachTop(margin: 8)
//        contentView.layoutAttachBottom(margin: 8)
//        contentView.layoutAttachLeading(margin: 16)
//        contentView.layoutAttachTrailing(margin: 16)
//        layer.cornerRadius = 10
//        self.backgroundColor = .clear
//        self.layoutIfNeeded()
//    }
}

open class DBTableViewCellParent<V>: UITableViewCell, DBTableViewCellDelegate {
    
    internal typealias T = V
    public var section: DBTableViewSection!
    internal weak var delegate: DBProtocol?
    
    open func registerHorizontalCell(configuration: DBCollectionViewCellConfig?) {
        
    }
    
    fileprivate func isForViewType(item: V) -> Bool {
        return false
    }
    
    internal func configureCell(item: V) {
        
    }
    
    internal func deleteButtonTapped(item: V) {
        
    }
    
    internal func insertButtonTapped(item: V) {
        
    }
    
    internal func cellDidSelect(item: V) {
        
    }
    
    internal func accessoryButtonTapped(item: V) {
        
    }
    
    internal func setDelegate(delegate: DBProtocol?) {
        self.delegate = delegate
    }
}


