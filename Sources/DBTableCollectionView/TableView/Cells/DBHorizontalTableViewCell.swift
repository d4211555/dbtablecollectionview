//
//  DBHorizontalTableViewCell.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 17.05.2021.
//

import Foundation
import UIKit

open class DBHorizontalTableViewCellConfig: DBTableViewCellConfig {
    open override func getHeight(tableView: UITableView, section: DBTableViewSection) -> CGFloat {
        var width = UIScreen.main.bounds.width
        if #available(iOS 13.0, *) {
            if (tableView.style == .insetGrouped) {
                width = width - 36
            }
        }
        return DBHelper.getCellHeight(collectionViewSize: CGSize(width: width, height: 0), section: section.horizontalSection) + 1
    }
}

open class DBHorizontalTableViewCell<M, V>: DBTableViewCell<M, V> where M: DBHorizontalItem {
    
    public var collectionView: DBCollectionView!
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = .clear
        collectionView = DBCollectionView()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        if let l = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            l.scrollDirection = .horizontal
        }
        collectionView.alwaysBounceVertical = false
        collectionView.backgroundColor = .clear
        self.contentView.addSubview(collectionView)
        collectionView.layoutAttachAll()
    }
    
    open class func getConfiguration(horizontalCellConfiguration: DBCollectionViewCellConfig? = nil) -> DBTableViewCellConfig {
        return DBHorizontalTableViewCellConfig(type: self.self, cellIdentifier: self.getClassName(), delegate: nil, modelType: M.self, registerType: .registerClass, nibName: nil, horizontalCellConfiguration: horizontalCellConfiguration)
    }
    
    public override func registerHorizontalCell(configuration: DBCollectionViewCellConfig?) {
        if configuration != nil {
            collectionView.registerCell(cell: configuration!)
        }
    }
    
    open override func configureCell(item: M) {
        //let s = section.horizontalSection
        guard let s = section.horizontalSection else {
            return
        }
        if (s.items.isEmpty) {
            s.items = item.items
        }
       
        collectionView.removeAll()
        collectionView.add(section: s)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    

    
}
