//
//  EmptyCartCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 07.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

public protocol DBEmptyTableViewCellDelegate: DBProtocol {
    
}

public class DBEmptyTableViewCellConfig: DBTableViewCellConfig {
    public override func getHeight(tableView: UITableView, section: DBTableViewSection) -> CGFloat {
        //let screenSize: CGRect = UIScreen.main.bounds
        return tableView.frame.height + tableView.contentOffset.y
    }
}

public class DBEmptyTableViewCell: DBTableViewCell<DBEmptyModel, DBEmptyTableViewCellDelegate> {
    
    @IBOutlet var emptyImageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var emptyImageViewHeight: NSLayoutConstraint!
    @IBOutlet var emptyImageViewTop: NSLayoutConstraint!
    
    public class func getConfiguration(delegate: DBEmptyTableViewCellDelegate?) -> DBTableViewCellConfig {
        super.getConfiguration(delegate: delegate, nibName: "DBEmptyTableViewCell", bundle: Bundle.module, registerType: .registerNib)
    }
    
    public override func configureCell(item: DBEmptyModel) {
        if (item.image != nil) {
            emptyImageView.image = item.image
            self.emptyImageViewHeight.constant = 150
            self.emptyImageViewTop.constant = 16
        }else{
            self.emptyImageViewHeight.constant = 0
            self.emptyImageViewTop.constant = 0
        }
        
        label.text = item.name
    }
}
