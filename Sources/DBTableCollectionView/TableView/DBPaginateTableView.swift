//
//  DBPaginateTableView.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 17.03.2021.
//

import Foundation
import UIKit

@objc public protocol DBPagingTableViewDelegate {
    
    @objc optional func didPaginate(_ tableView: DBPagingTableView, to page: Int)
    func paginate(_ tableView: DBPagingTableView, to page: Int)
    
}

open class DBPagingTableView: DBTableView {
    
    private var loadingView: UIView!
    private var indicator: UIActivityIndicatorView!
    internal var page: Int = 1
    internal var previousItemCount: Int = 0
    var totalCount: Int?
    
    open var currentPage: Int {
        get {
            return page
        }
    }
    
    open weak var pagingDelegate: DBPagingTableViewDelegate? {
        didSet {
            pagingDelegate?.paginate(self, to: page)
        }
    }
    
    open var isLoading: Bool = false {
        didSet {
            isLoading ? showLoading() : hideLoading()
        }
    }
    
    open func reset() {
        //self.removeAll()
        page = 1
        previousItemCount = 0
        pagingDelegate?.paginate(self, to: page)
    }
    
    
    
    private func paginate(_ tableView: DBPagingTableView, forIndexAt indexPath: IndexPath) {
        if (isGlobalEmptyCell) {
            return
        }
        let itemCount = tableView.dataSource?.tableView(tableView, numberOfRowsInSection: indexPath.section) ?? 0
        if (totalCount != nil && itemCount == totalCount) {
            return
        }
        guard indexPath.row == itemCount - 1 else { return }
        guard previousItemCount != itemCount else { return }
        page += 1
        previousItemCount = itemCount
        pagingDelegate?.paginate(self, to: page)
    }
    
    private func showLoading() {
        if loadingView == nil {
            createLoadingView()
        }
        tableFooterView = loadingView
    }
    
    private func hideLoading() {
        //reloadData()
        pagingDelegate?.didPaginate?(self, to: page)
        tableFooterView = UIView()
    }
    
    private func createLoadingView() {
        loadingView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 50))
        indicator = UIActivityIndicatorView()
        indicator.color = UIColor.lightGray
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.startAnimating()
        loadingView.addSubview(indicator)
        centerIndicator()
        //tableFooterView = loadingView
    }
    
    private func centerIndicator() {
        let xCenterConstraint = NSLayoutConstraint(
            item: loadingView!, attribute: .centerX, relatedBy: .equal,
            toItem: indicator, attribute: .centerX, multiplier: 1, constant: 0
        )
        loadingView.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(
            item: loadingView!, attribute: .centerY, relatedBy: .equal,
            toItem: indicator, attribute: .centerY, multiplier: 1, constant: 0
        )
        loadingView.addConstraint(yCenterConstraint)
    }
    
    override open func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
        paginate(self, forIndexAt: indexPath)
        return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
    }
    
}
