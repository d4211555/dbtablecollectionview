//
//  DBTableViewSection.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 17.05.2021.
//

import Foundation
import UIKit

open class DBTableViewSection: DBSection {
    public var headerTitle: String? = nil
    public var headerView: UIView? = nil
    public var headerHeight: CGFloat = 1
    public var footerTitle: String? = nil
    public var footerView: UIView? = nil
    public var footerHeight: CGFloat = 1
    var horizontalSection: DBCollectionViewSection!
    override public init() {
        super.init()
    }
    
    public init(headerTitle: String? = nil, items: [DBModel], headerHeight: CGFloat = 1) {
        super.init()
        self.headerTitle = headerTitle
        self.items = items
        self.headerHeight = headerHeight
        if (headerTitle != "") {
            self.headerHeight = UITableView.automaticDimension
        }
    }
    
    public init(headerTitle: String? = nil, items: [DBModel], horizontalSection: DBCollectionViewSection, headerHeight: CGFloat = 1) {
        super.init()
        self.headerTitle = headerTitle
        self.items = items
        self.horizontalSection = horizontalSection
        self.headerHeight = headerHeight
        if (headerTitle != "") {
            self.headerHeight = UITableView.automaticDimension
        }
        //
    }
    
    public init(items: [DBModel], headerHeight: CGFloat = 1) {
        super.init()
        self.items = items
        self.headerHeight = headerHeight
    }
    
    public init(items: [DBModel], horizontalSection: DBCollectionViewSection, headerHeight: CGFloat = 1) {
        super.init()
        self.items = items
        self.horizontalSection = horizontalSection
        self.headerHeight = headerHeight
        if (headerTitle != "") {
            self.headerHeight = UITableView.automaticDimension
        }
    }
   
}

