//
//  UIView+Extensions.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 13.05.2021.
//

import Foundation
import UIKit

extension UIView {
    public func layoutAttachAll(margin : CGFloat = 0.0) {
        let view = superview
        layoutAttachTop(to: view, margin: margin)
        layoutAttachBottom(to: view, margin: margin, priority: nil)
        layoutAttachLeading(to: view, margin: margin)
        layoutAttachTrailing(to: view, margin: margin)
    }
    
    public func layoutAttachAll(top : CGFloat = 0.0, bottom : CGFloat = 0.0, right : CGFloat = 0.0, left : CGFloat = 0.0) {
        let view = superview
        layoutAttachTop(to: view, margin: top)
        layoutAttachBottom(to: view, margin: bottom, priority: nil)
        layoutAttachLeading(to: view, margin: left)
        layoutAttachTrailing(to: view, margin: right)
    }
    
    /// attaches the top of the current view to the given view's top if it's a superview of the current view, or to it's bottom if it's not (assuming this is then a sibling view).
    /// if view is not provided, the current view's super view is used
    
    @discardableResult
    public func layoutAttachTop(to: UIView? = nil, margin : CGFloat = 0.0, relatedBy: NSLayoutConstraint.Relation = .equal) -> NSLayoutConstraint {
        
        let view: UIView? = to ?? superview
        let isSuperview = view == superview
        let constraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: relatedBy, toItem: view, attribute: isSuperview ? .top : .bottom, multiplier: 1.0, constant: margin)
        superview?.addConstraint(constraint)
        
        return constraint
    }
    
    
    /// attaches the bottom of the current view to the given view
    @discardableResult
    public func layoutAttachBottom(to: UIView? = nil, margin : CGFloat = 0.0, priority: UILayoutPriority? = nil, relatedBy: NSLayoutConstraint.Relation = .equal) -> NSLayoutConstraint {
        
        let view: UIView? = to ?? superview
        let isSuperview = (view == superview) || false
        let constraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: relatedBy, toItem: view, attribute: isSuperview ? .bottom : .top, multiplier: 1.0, constant: -margin)
        if let priority = priority {
            constraint.priority = priority
        }
        superview?.addConstraint(constraint)
        
        return constraint
    }
    
    @discardableResult
    public func layoutAttachHeight(constant : CGFloat = 0.0) -> NSLayoutConstraint {
        
        let constraint = self.heightAnchor.constraint(equalToConstant: constant)
        constraint.isActive = true
        
        return constraint
    }
    
    @discardableResult
    public func layoutAttachWidth(constant : CGFloat = 0.0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {
        
        let constraint = self.widthAnchor.constraint(equalToConstant: constant)
        if let priority = priority {
            constraint.priority = priority
        }
        constraint.isActive = true
        
        return constraint
    }
    
    /// attaches the leading edge of the current view to the given view
    @discardableResult
    public func layoutAttachLeading(to: UIView? = nil, margin : CGFloat = 0.0) -> NSLayoutConstraint {
        
        let view: UIView? = to ?? superview
        let isSuperview = (view == superview) || false
        let constraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: isSuperview ? .leading : .trailing, multiplier: 1.0, constant: margin)
        superview?.addConstraint(constraint)
        
        return constraint
    }
    
    /// attaches the trailing edge of the current view to the given view
    @discardableResult
    public func layoutAttachTrailing(to: UIView? = nil, margin : CGFloat = 0.0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {
        
        let view: UIView? = to ?? superview
        let isSuperview = (view == superview) || false
        let constraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: isSuperview ? .trailing : .leading, multiplier: 1.0, constant: -margin)
        if let priority = priority {
            constraint.priority = priority
        }
        superview?.addConstraint(constraint)
        
        return constraint
    }
}
