//
//  AdapterModel.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

open class DBModel: NSObject {
    
    public var deleteButtonTitle: String?
    public var canEditing: Bool?
    public var canMove: Bool?
    public var editingStyle: UITableViewCell.EditingStyle?
    public var height: CGFloat?
    public var collectionViewItemSize: CGSize?
    
    static func == (lhs: DBModel, rhs: DBModel) -> Bool {
        
        let firstId = Mirror(reflecting: lhs).children.first{$0.label == "id"}?.value
        let secondId = Mirror(reflecting: rhs).children.first{$0.label == "id"}?.value
        if (firstId == nil || secondId == nil) {
            return lhs.isEqual(rhs)
        }
        
        let s1 = NSStringFromClass(type(of: lhs)) + "_\(firstId as! Int)"
        let s2 = NSStringFromClass(type(of: rhs)) + "_\(secondId as! Int)"
        if ("\(s1)_\(firstId as! Int)" != "\(s2)_\(secondId as! Int)") {
            return false
        }
        
        return true
    }
    static public func getClassName() -> String {
        return String(describing: type(of: self))
    }
    
}

open class DBSupplementaryModel: NSObject {
    
}



open class DBSection: NSObject {
    
    internal var items = [DBModel]()
    
    public var emptyModel = DBEmptyModel()
    public var retryModel = DBRetryModel()
    
    public func setEmptyModel(name: String, image: UIImage?, height: CGFloat = 50) {
        emptyModel = DBEmptyModel(name: name, image: image, height: height)
    }

    public func setRetryModel(name: String, image: UIImage?, height: CGFloat = 50) {
        retryModel = DBRetryModel(name: name, image: image, height: height)
    }
    
    public override init() {
        
    }
    
    func getItems() -> [DBModel] {
        return self.items.filter { (model) -> Bool in
            return !(model is DBEmptyModel) && !(model is DBRetryModel)
        }
    }
    
    
   
}

public extension Array where Element: Equatable {
    func indexById<T>(object: T) -> Int? where T : DBModel {
        var index: Int? = nil
        for (i, element) in self.enumerated() {
            if (element as! DBModel == object) {
                index = i
                break
            }
        }
        return index
    }
}

internal extension Array where Element: Equatable {
    
//    // Remove first collection element that is equal to the given `object`:
    mutating func removeById<T>(object: T) -> Int? where T : DBModel{
        guard let index = index(object: object) else {return nil}
        remove(at: index)
        return index
    }
    
    mutating func remove<T>(type: T.Type) -> Int? where T : DBModel{
        guard let index = firstIndex(where: {$0 is T}) else {return nil}
        remove(at: index)
        return index
    }
    
    mutating func remove(object: Element) -> Int?{
        guard let index = firstIndex(of: object) else {return nil}
        remove(at: index)
        return index
    }


    func indexByAddress(object: Element) -> Int?{
        guard let index = firstIndex(of: object) else {return nil}
        return index
    }
    
    
    func index<T>(object: T) -> Int? where T : DBModel {
        var index: Int? = nil
        for (i, element) in self.enumerated() {
            if (element as! DBModel == object) {
                index = i
                break
            }
        }
        return index
    }
    
    
}

open class DBEmptyModel: DBModel {
    var id = -111
    public var name: String = "Data is empty"
    public var image: UIImage?
    
    override init() {
        
    }
    
    public init(name: String, image: UIImage?, height: CGFloat) {
        super.init()
        self.name = name
        self.image = image
        self.height = height
    }
}

open class DBRetryModel: DBModel {
    var id = -112
    public var image: UIImage?
    public var name: String = "Internal Server Error"

    override init() {
        
    }
    
    public init(name: String, image: UIImage?, height: CGFloat) {
        super.init()
        self.name = name
        self.image = image
        self.height = height
    }
    
}

//public protocol DBCellProtocol: class{
//
//}

//public protocol DBSupplementaryViewProtocol: class{
//
//}


public enum RegisterCellType {
    case registerClass
case registerNib
    case none
}
