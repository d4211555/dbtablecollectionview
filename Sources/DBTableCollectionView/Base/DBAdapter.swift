//
//  DBAdapter.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 30.03.2021.
//

import Foundation

internal class DBAdapterParent: NSObject {
    
   
    
}

@objc public protocol DBProtocol: AnyObject {
    
}

internal protocol DBCellDelegate: class {
    associatedtype T
    func configureCell(item: T)
    func isForViewType(item: T) -> Bool
    func cellDidSelect(item: T)
    func deleteButtonTapped(item: T)
    func insertButtonTapped(item: T)
}

internal protocol DBSupplementaryDelegate: class {
    associatedtype T
    func configureView(item: T)
}


