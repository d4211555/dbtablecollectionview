//
//  DBCollectionViewSection.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 30.03.2021.
//

import Foundation
import UIKit

open class DBCollectionViewSection: DBSection {
    
    public var supplementaryViewItem: DBSupplementaryModel? = nil
    //MARK:- Config var
    var itemSpacing: CGFloat!
    var lineSpacing: CGFloat!
    var groupSpacing: CGFloat!
    var insets: UIEdgeInsets!
    var widthRatio: CGFloat!
    public var height: CGFloat?
    public var heightRatio: CGFloat? = 0.61
    var itemCount: Int!
    var headerKind: String? = nil
    var scrollBehavior: DBCollectionLayoutSectionOrthogonalScrollingBehavior!
    var horizontalTableView: Bool!
    
    var scrollInterval: Double!
    var isAutoScroll: Bool = false
    
    public func setConfigutarion(itemSpacing: CGFloat = 10, lineSpacing: CGFloat = 10, groupSpacing: CGFloat = 10, insets: UIEdgeInsets = UIEdgeInsets(top: 10, left: 16, bottom: 10, right: 16), widthRatio: CGFloat = 1.0, height: CGFloat? = nil, heightRatio: CGFloat? = nil, itemCount: Int, isHeader: Bool = false, scrollBehavior: DBCollectionLayoutSectionOrthogonalScrollingBehavior = .none, horizontalTableView: Bool = false) {
        self.itemSpacing = itemSpacing
        self.lineSpacing = lineSpacing
        self.groupSpacing = groupSpacing
        self.insets = insets
        self.widthRatio = widthRatio
        self.height = height
        self.heightRatio = heightRatio
        self.itemCount = itemCount
        if (isHeader) {
            self.headerKind = UICollectionView.elementKindSectionHeader
        }
        self.scrollBehavior = scrollBehavior
        self.horizontalTableView = horizontalTableView
    }
    
    
    
    public func setAutoScroll(interval: Double) {
        self.isAutoScroll = true
        self.scrollInterval = interval
    }
    
    open func getConfiguration(isHeader: Bool = false) {
        self.setConfigutarion(insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), heightRatio: 2.0, itemCount: 1, isHeader: isHeader, scrollBehavior: .none)
    }
    
    open func getHorizontalConfiguration() -> (itemSpacing: CGFloat, lineSpacing: CGFloat, sectionInset: UIEdgeInsets)? {
        return nil
    }
    
    public override init() {
        super.init()
        self.getConfiguration()
    }
    
    public init(items: [DBModel]) {
        super.init()
        self.items = items
        self.getConfiguration()
    }
    
    //    @available(iOS 13.0, *)
    //    open func getSectionConfiguration(layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection {
    //        return getSectionConfiguration(layoutEnvironment: layoutEnvironment, itemCount: 2, headerKind: nil)
    //    }
    
    @available(iOS 13.0, *)
    internal func getSectionConfiguration(layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection{
        
        if (items.count == itemCount) {
            self.scrollBehavior = DBCollectionLayoutSectionOrthogonalScrollingBehavior.none
        }
        
        var scrollBehavior = UICollectionLayoutSectionOrthogonalScrollingBehavior(rawValue: self.scrollBehavior.rawValue)
        var h = NSCollectionLayoutDimension.estimated(100)
        if (height != nil) {
            h = NSCollectionLayoutDimension.absolute(height!)
        }else if (heightRatio != nil){
            let width = DBHelper.getCellWidth(collectionViewSize: layoutEnvironment.container.contentSize, section: self)
            h = NSCollectionLayoutDimension.absolute(CGFloat(width) * heightRatio!)
        }
        let leadingItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0), heightDimension: h))
        var containerGroup: NSCollectionLayoutGroup!
        containerGroup = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(widthRatio),
                                               heightDimension: h),
            subitem: leadingItem, count: itemCount)
        containerGroup.interItemSpacing = NSCollectionLayoutSpacing.fixed(itemSpacing)
        return initSection(containerGroup: containerGroup, headerKind: headerKind, insets: insets, groupSpacing: groupSpacing, scrollBehavior: scrollBehavior!)
    }

    @available(iOS 13.0, *)
    private func initSection(containerGroup: NSCollectionLayoutGroup, headerKind: String?, insets: UIEdgeInsets, groupSpacing: CGFloat, scrollBehavior: UICollectionLayoutSectionOrthogonalScrollingBehavior) -> NSCollectionLayoutSection {
        let section = NSCollectionLayoutSection(group: containerGroup)
        section.orthogonalScrollingBehavior = scrollBehavior
        if let kind = headerKind{
            let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                   heightDimension: .estimated(44)),
                elementKind: kind,
                alignment: .top)
            section.boundarySupplementaryItems = [sectionHeader]
        }
        section.interGroupSpacing = groupSpacing
        section.contentInsets = NSDirectionalEdgeInsets(top: insets.top, leading: insets.left, bottom: insets.bottom, trailing: insets.right)
        return section
    }
}

public enum DBCollectionLayoutSectionOrthogonalScrollingBehavior : Int {
    case none = 0
    case continuous = 1
    case continuousGroupLeadingBoundary = 2
    case paging = 3
    case groupPaging = 4
    case groupPagingCentered = 5
}
