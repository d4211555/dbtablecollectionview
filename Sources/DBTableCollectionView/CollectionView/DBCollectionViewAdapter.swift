//
//  DBCollectionViewAdapter.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 30.03.2021.
//

import Foundation
import UIKit


internal class DBCollectionViewAdapter: DBAdapterParent, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    // typealias M = T
    internal var cells = [DBCollectionViewCellConfig]()
    internal var supplementaryViews = [DBSupplementaryViewConfig]()
    
    //internal var items = [DBModel]()
    internal weak var delegate: DBCollectionViewAdapterDelegate?
    
    internal var sections = [DBCollectionViewSection]()
    
    private var collectionView: UICollectionView!
    
    private var _dataSource: Any? = nil
    @available(iOS 13.0, *)
    open var dataSource: UICollectionViewDiffableDataSource<DBCollectionViewSection, DBModel>? {
        get {
            return _dataSource as? UICollectionViewDiffableDataSource
        } set {
            _dataSource = newValue
        }
    }
    private var _snapshot: Any? = nil
    @available(iOS 13.0, *)
    open var snapshot: NSDiffableDataSourceSnapshot<DBCollectionViewSection, DBModel>? {
        get {
            return _snapshot as? NSDiffableDataSourceSnapshot
        } set {
            _snapshot = newValue
        }
    }
    
    init(collectionView: UICollectionView) {
        super.init()
        self.collectionView = collectionView
        if #available(iOS 13.0, *) {
            self.configureDataSource()
        }
    }
    
    @available(iOS 13.0, *)
    private func configureDataSource() {
        snapshot = NSDiffableDataSourceSnapshot<DBCollectionViewSection, DBModel>()
        dataSource = UICollectionViewDiffableDataSource<DBCollectionViewSection, DBModel>(collectionView: collectionView) { (collectionView, indexPath, model) in
            // let section = self.sections[indexPath.section]
            return self.getCell(indexPath: indexPath, collectionView: collectionView)
            
        }
        dataSource!.supplementaryViewProvider = { collectionView, kind, indexPath in
            return self.getSupplementaryView(collectionView: collectionView, indexPath: indexPath, kind: kind)
        }
    }
    
    
    internal func findSection(by item: DBModel) -> (section: DBCollectionViewSection, index: Int)? {
        if (sections.count == 0) {
            return nil
        }
        let i = sections.firstIndex { (section) -> Bool in
            return section.items.index(object: item) != nil
        }
        if (i != nil) {
            return (sections[i!], i!)
        }
        return nil
    }
    
    internal func getItem(indexPath: IndexPath) -> DBModel {
        let section = self.sections[indexPath.section]
        let item = section.items[indexPath.row]
        return item
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll?(scrollView)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidEndDecelerating?(scrollView)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return getCell(indexPath: indexPath, collectionView: collectionView)!
    }
    
    func getCell(indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell? {
        let item = getItem(indexPath: indexPath)
        if (item == nil) {
            return nil
        }
        let config = getCellForIndexPath(item: item)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.cellIdentifier, for: indexPath) as! DBCollectionViewCellParent<DBModel>
        cell.delegate = config.delegate
        cell.section = self.sections[indexPath.section]
        cell.configureCell(item: item)
        return cell
    }
    
    internal func getCellForIndexPath(item: DBModel) -> DBCollectionViewCellConfig {
        var cell: DBCollectionViewCellConfig?
        cells.forEach { (it) in
            if it.modelType == type(of: item) {
                cell = it
            }
        }
        if (cell == nil) {
            fatalError("There is no registered cell for \(item.self)")
        }
        return cell!
    }
    
    internal func getSupplementatyViewForIndexPath(item: DBSupplementaryModel) -> DBSupplementaryViewConfig {
        var view: DBSupplementaryViewConfig?
        supplementaryViews.forEach { (it) in
            if it.modelType == type(of: item) {
                view = it
            }
        }
        if (view == nil) {
            fatalError("There is no registered supplementary view for \(item.self)")
        }
        return view!
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //print("sizeForItemAt - \(sections[indexPath.section].self)")
        let item = getItem(indexPath: indexPath)
        if let value = item.collectionViewItemSize {
            return value
        }else{
            return getCellForIndexPath(item: getItem(indexPath: indexPath)).getSize(collectionView: collectionView, defautlItemSize: (collectionViewLayout as! UICollectionViewFlowLayout).itemSize, section: self.sections[indexPath.section])
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        (collectionView.cellForItem(at: indexPath) as! DBCollectionViewCellParent).cellDidSelect(item: getItem(indexPath: indexPath))
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        if (section.isAutoScroll) {
            //timerIndexed["\(section.self)"] = indexPath.row
          //  print("TTTT - willDisplay(\(section.self) - \(indexPath.row)")
            startTimer(section: section, interval: section.scrollInterval)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return self.getSupplementaryView(collectionView: collectionView, indexPath: indexPath, kind: kind)
    }
    
    func getSupplementaryView(collectionView: UICollectionView, indexPath: IndexPath, kind: String) -> UICollectionReusableView{
        let section = sections[indexPath.section]
        guard let supplementaryViewItem = section.supplementaryViewItem else {
            return UICollectionReusableView()
        }
        let viewConfig = getSupplementatyViewForIndexPath(item: supplementaryViewItem)
        if kind != viewConfig.kind {
            return UICollectionReusableView()
        }
        
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: viewConfig.kind, withReuseIdentifier: viewConfig.viewIdentifier, for: indexPath) as! DBSupplementaryViewParent<DBSupplementaryModel>
        
        view.configureView(item: section.supplementaryViewItem!)
        return view
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let s = self.sections[section]
        if let config = s.getHorizontalConfiguration() {
            return config.sectionInset
        }else {
            return s.insets
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let s = self.sections[section]
        if let config = s.getHorizontalConfiguration() {
            return config.lineSpacing
        }else {
            return s.lineSpacing
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        let s = self.sections[section]
        //if let config = s.getHorizontalConfiguration() {
        //    return config.itemSpacing
        //}else {
        return s.itemSpacing
        //}
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let s = sections[section]
        if (s.supplementaryViewItem != nil) {
            let config = getSupplementatyViewForIndexPath(item: s.supplementaryViewItem!)
            if (config.automaticSize()) {
                let indexPath = IndexPath(row: 0, section: section)
                let headerView = getSupplementaryView(collectionView: collectionView, indexPath: indexPath, kind: UICollectionView.elementKindSectionHeader)
                return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),
                                                          withHorizontalFittingPriority: .required, // Width is fixed
                                                          verticalFittingPriority: .fittingSizeLevel)
            }
            return config.getSize(collectionViewSize: collectionView.frame.size)
        }
        return CGSize.zero
    }
    
    // -MARK: Stocks Timer
    var timers = Dictionary<String, Timer>()
    var timerIndexed = Dictionary<String, Int>()
    var lastIndexes = Dictionary<String, Bool>()
    func startTimer(section: DBCollectionViewSection, interval: Double) {
        if (timers["\(section.self)"] != nil) {
            return
        }
        let timer =  Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { timer in
            self.scroll(section: section)
        }
        timers["\(section.self)"] = timer
    }
    
    @objc func scroll(section: DBCollectionViewSection){
        if (section.items.isEmpty) {
            return
        }
//        var index: Int! = timerIndexed["\(section.self)"]
//        if (index == nil) {
//            index = 0
//        }
        let indexSection = sections.index { el in
            return el == section
        }
        if (indexSection == nil) {
            return
        }
        
        //let visibleCells = collectionView.indexPathsForVisibleItems
        let visibleCells = collectionView.indexPathsForVisibleItems
        
        if (visibleCells.first{$0.section == indexSection!} != nil) {
            let sectionVisibleCells = visibleCells.filter{$0.section == indexSection!}
            var index = sectionVisibleCells.max{$0.row < $1.row}!.row
            //index = index >= section.items.count - 1 ? 0 : index

//            if (lastIndexes["\(section.self)"] != nil && lastIndexes["\(section.self)"] == true) {
//               index = 0
//            }
            
            UIView.animate(withDuration: 1, animations: { [weak self] in
                self?.collectionView.scrollToItem(at: IndexPath.init(row: index, section: indexSection!), at: .left, animated: false)
            })
//            if (index >= section.items.count - 1) {
//                lastIndexes["\(section.self)"] = true
//            }else{
//                lastIndexes["\(section.self)"] = false
//            }
        }
    }
}
