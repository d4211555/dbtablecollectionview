//
//  AdapterTableViewCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 09.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

open class DBCollectionViewCell<B: DBModel, D>: DBCollectionViewCellParent<DBModel> {
    public var id = 0
    public var item: B!
  
    public func getBundle() -> Bundle {
        return Bundle.module
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.layoutAttachAll()
        
    }
    
public class func getConfiguration(delegate: D?, nibName: String?, bundle: Bundle? = nil, registerType: RegisterCellType = .registerClass) -> DBCollectionViewCellConfig{
        let className = DBCollectionViewCell.getClassName()
        return DBCollectionViewCellConfig(type: self.self, cellIdentifier: className, delegate: (delegate as? DBProtocol), modelType: B.self, registerType: registerType, bundle: bundle, nibName: nibName)
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.layoutAttachAll()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func isForViewType(item: DBModel) -> Bool {
        return item is B
    }
    
    override internal func configureCell(item: DBModel) {
        self.item = (item as! B)
        
        configureCell(item: item as! B)
    }
    
    override internal func deleteButtonTapped(item: DBModel) {
        deleteButtonTapped(item: item as! B)
    }
    
    override internal func cellDidSelect(item: DBModel) {
        cellDidSelect(item: item as! B)
    }
    
    override internal func insertButtonTapped(item: DBModel) {
        insertButtonTapped(item: item as! B)
    }
    
    open func configureCell(item: B) {
       
    }
    
    open func deleteButtonTapped(item: B) {
        
    }
    
    open func insertButtonTapped(item: B) {
        
    }
    
    open func cellDidSelect(item: B) {
        
    }
    
    open func getDelegate() -> D? {
        return delegate as? D
    }
    
    public class func getClassName() -> String {
        return String(describing: type(of: self))
    }
//    open class func getConfiguration<D: DBProtocol>(delegate: D?, registerType: RegisterCellType = .none) -> DBCollectionViewCellConfig{
//        return DBCollectionViewCellConfig(type: DBCollectionViewCell.self, cellIdentifier: "CatalogItemCell", delegate: delegate, modelType: B.self, registerType: registerType)
//    }
    
}

open class DBCollectionViewCellParent<V>: UICollectionViewCell, DBCellDelegate {
    
    internal typealias T = V
    public var section: DBCollectionViewSection!
    internal weak var delegate: DBProtocol?
    
    internal func isForViewType(item: V) -> Bool {
        return false
    }
    
    internal func configureCell(item: V) {
        
    }
    
    internal func willDisplay() {
        
    }
    
    internal func deleteButtonTapped(item: V) {
        
    }
    
    internal func insertButtonTapped(item: V) {
        
    }
    
    internal func cellDidSelect(item: V) {
        
    }
    
    internal func setDelegate(delegate: DBProtocol?) {
        self.delegate = delegate
    }
}

open class DBSupplementaryView<B: DBSupplementaryModel, D>: DBSupplementaryViewParent<DBSupplementaryModel> {
    public var item: B!
    
    override internal func configureView(item: DBSupplementaryModel) {
        self.item = (item as! B)
        configureView(item: item as! B)
    }
    
    open func configureView(item: B) {
        
    }
    
}

open class DBSupplementaryViewParent<V>: UICollectionReusableView, DBSupplementaryDelegate {
    internal typealias T = V

    internal func configureView(item: V) {
        
    }
}
