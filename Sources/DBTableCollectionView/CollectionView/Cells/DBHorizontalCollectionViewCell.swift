//
//  DBHorizontalCell.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 12.04.2021.
//

import Foundation
import QuartzCore
import UIKit

open class DBHorizontalItem: DBModel {
    var items = [DBModel]()
    var autoScrollInterval: Double?
    public init(items: [DBModel], autoScrollInterval: Double? = nil) {
        self.items = items
        self.autoScrollInterval = autoScrollInterval
    }
    
    public func getItemCount() -> Int{
        return items.count
    }
}

open class DBHorizontalCollectionViewCellConfig: DBCollectionViewCellConfig {
//    override func initCell() -> MainAdapterTableViewCell<Category, CatalogItemCellDelegate, AdapterModel>? {
//        return CatalogItemCell.fromNib(nibName: "CatalogItemCell")
//    }
    open override func getSize(collectionView: UICollectionView, defautlItemSize: CGSize, section: DBCollectionViewSection) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: DBHelper.getCellHeight(collectionViewSize: collectionView.frame.size, section: section) + 1)
    }
}


open class DBHorizontalCollectionViewCell<M, V>: DBCollectionViewCell<M, V> where M: DBHorizontalItem {
    public var collectionView: DBCollectionView!

    
    public override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        collectionView = DBCollectionView()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        collectionView.backgroundColor = UIColor.white
        contentView.addSubview(collectionView)
        //collectionView.isPagingEnabled = true
        collectionView.layoutAttachAll()
        //self.frame.size = CGSize(width: UIWindow.appearance().frame.size.width, height: 250)

    }
    
    open override func configureCell(item: M) {
        let s = DBCollectionViewSection(items: (item as DBHorizontalItem).items)
        s.groupSpacing = section.groupSpacing
        s.lineSpacing = section.lineSpacing
        s.itemSpacing = section.itemSpacing
        s.insets = section.insets
        s.widthRatio = section.widthRatio
        s.height = section.height
        s.heightRatio = section.heightRatio
        s.itemCount = section.itemCount
        s.headerKind = section.headerKind
        s.scrollBehavior = section.scrollBehavior
        if (item.autoScrollInterval != nil){
            s.setAutoScroll(interval: item.autoScrollInterval!)
        }
        
        collectionView.removeAll()
        collectionView.add(section: s)
        
    }


//    open class func getConfiguration<V>(delegate: V) -> DBCollectionViewCellConfig where V: DBProtocol, V : UIViewController{
//        return DBCollectionViewCellConfig(type: DBHorizontalCell.self, cellIdentifier: "DBHorizontalCell", delegate: delegate, modelType: DBHorizontalItem.self, registerType: .registerClass)
//    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CyclicCardFlowLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let targetRect = CGRect(x: proposedContentOffset.x, y: 0.0, width:  self.collectionView!.bounds.size.width, height: self.collectionView!.bounds.size.height)
        let attriArray = super.layoutAttributesForElements(in: targetRect)! as [UICollectionViewLayoutAttributes]
        let horizontalCenterX = proposedContentOffset.x + (self.collectionView!.bounds.width / 2.0)
        var offsetAdjustment = CGFloat(MAXFLOAT)
        for layoutAttributes in attriArray {
            let itemHorizontalCenterX = layoutAttributes.center.x
            if(abs(itemHorizontalCenterX-horizontalCenterX) < abs(offsetAdjustment)){
                offsetAdjustment = itemHorizontalCenterX-horizontalCenterX
            }
        }
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
    
    let ActiveDistance : CGFloat = 400 //垂直缩放除以系数
    let ScaleFactor : CGFloat = 0.25     //缩放系数  越大缩放越大
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        let array = super.layoutAttributesForElements(in: rect)
        var visibleRect = CGRect()
        visibleRect.origin = self.collectionView!.contentOffset
        visibleRect.size = self.collectionView!.bounds.size
        
        for attributes in array!{
            let distance = visibleRect.midX - attributes.center.x
            let normalizedDistance = abs(distance/ActiveDistance)
            let zoom = 1 - ScaleFactor * normalizedDistance
            attributes.transform3D = CATransform3DMakeScale(1.0, zoom, 1.0)
            attributes.zIndex = 1
            let alpha = 1 - normalizedDistance
            attributes.alpha = alpha
        }
        return array
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
