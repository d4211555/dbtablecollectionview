//
//  EmptyCartCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 07.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

public protocol DBEmptyCollectionViewCellDelegate: DBProtocol {
    
}

public class DBEmptyCollectionViewCellConfig: DBCollectionViewCellConfig {
//    public override func getHeight(tableViewHeight: CGFloat, section: DBTableViewSection) -> CGFloat {
//        //let screenSize: CGRect = UIScreen.main.bounds
//        return tableViewHeight
//    }
    public override func getSize(collectionView: UICollectionView, defautlItemSize: CGSize, section: DBCollectionViewSection) -> CGSize {
        return collectionView.frame.size
    }
}

public class DBEmptyCollectionViewCell: DBCollectionViewCell<DBEmptyModel, DBEmptyCollectionViewCellDelegate> {
    
    @IBOutlet var emptyImageView: UIImageView!
    @IBOutlet var label: UILabel!
    
    public class func getConfiguration(delegate: DBEmptyTableViewCellDelegate?) -> DBCollectionViewCellConfig {
        return DBEmptyCollectionViewCellConfig(type: DBEmptyCollectionViewCell.self, cellIdentifier: "DBEmptyCollectionViewCell", delegate: nil, modelType: DBEmptyModel.self, registerType: .registerNib, bundle: Bundle.module, nibName: "DBEmptyCollectionViewCell")
    }
    
    public override func configureCell(item: DBEmptyModel) {
        if (item.image != nil) {
            emptyImageView.image = item.image
        }
        
        label.text = item.name
    }
}
