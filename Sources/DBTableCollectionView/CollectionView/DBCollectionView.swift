//
//  DBCollectionView.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 30.03.2021.
//

import Foundation
import UIKit

@objc public protocol DBCollectionViewAdapterDelegate: NSObjectProtocol {
    @objc optional func scrollViewDidScroll(_ scrollView: UIScrollView)
    @objc optional func moved(item: DBModel)
    @objc optional func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    @available(iOS 13.0, *)
    @objc optional func visibleItemsInvalidation(visibleItems: [NSCollectionLayoutVisibleItem])
}

open class DBCollectionView: UICollectionView{
    public weak var collectionViewDelegate: DBCollectionViewAdapterDelegate?  {
        didSet {
            adapter.delegate = collectionViewDelegate
        }
    }
    internal var adapter: DBCollectionViewAdapter! {
        didSet {
            if #available(iOS 13.0, *) {
            }else{
                self.dataSource = adapter
            }
            self.delegate = adapter
            
            //adapter.delegate = tableViewDelegate
        }
    }
    private var emptyModel = DBEmptyModel()
    private var retryModel = DBRetryModel()
    
    var isGlobalEmptyCell = false
    var isGlobalRetryCell = false
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        configureTableView()
    }
    
    public init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        configureTableView()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        configureTableView()
        //fatalError("init(coder:) has not been implemented")
    }
    
    public func setEmptyModel(name: String, image: UIImage?, height: CGFloat) {
        emptyModel = DBEmptyModel(name: name, image: image, height: height)
    }
    
    public func setRetryModel(name: String, image: UIImage?, height: CGFloat) {
        retryModel = DBRetryModel(name: name, image: image, height: height)
    }
    
    private func configureTableView() {
        adapter = DBCollectionViewAdapter(collectionView: self)
        //self.reloadData()
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        if #available(iOS 13.0, *) {
            collectionViewLayout = createLayout()
        } else {
            let l = UICollectionViewFlowLayout()
            // let s = self.frame.size.width
            
            //if #available(iOS 10.0, *) {
            // l.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
            
            //}
            collectionViewLayout = l
        }
        //collectionViewLayout.invalidateLayout()
    }
    
    @available(iOS 13.0, *)
    func createLayout(sectionSpacing: CGFloat = 10) -> UICollectionViewLayout {
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = sectionSpacing
        
        let layout = UICollectionViewCompositionalLayout(sectionProvider: {
            (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            if self.getSections().isEmpty {return nil}
            let sec = self.getSections()[sectionIndex]
            
            let c = sec.getSectionConfiguration(layoutEnvironment: layoutEnvironment)
            c.visibleItemsInvalidationHandler = { [weak self] visibleItems, point, environment in
                self?.collectionViewDelegate?.visibleItemsInvalidation?(visibleItems: visibleItems)
            }
            return c
        }, configuration: config)
        return layout
    }
    
    
    
    public func getItems() -> [DBModel] {
        return self.adapter.sections.reduce(into: [DBModel]()) { (itemss, section) in
            itemss.append(contentsOf: section.getItems())
        }
    }
    
    public func getSections() -> [DBCollectionViewSection] {
        return self.adapter.sections
    }
    
    
    
    public func registerCell(cell: DBCollectionViewCellConfig){
        adapter.cells.append(cell)
        switch cell.registerType {
        case .registerClass:
            self.register(cell.type, forCellWithReuseIdentifier: cell.cellIdentifier)
        case .registerNib:
            self.register(UINib(nibName:cell.nibName!, bundle: cell.bundle), forCellWithReuseIdentifier: cell.cellIdentifier)
        default:
            print("")
        }
    }
    
    public func registerSupplementaryView(viewConfig: DBSupplementaryViewConfig){
        adapter.supplementaryViews.append(viewConfig)
        switch viewConfig.registerType {
        case .registerClass:
            self.register(viewConfig.type, forSupplementaryViewOfKind: viewConfig.kind, withReuseIdentifier: viewConfig.viewIdentifier)
        case .registerNib:
            self.register(UINib(nibName:viewConfig.viewIdentifier, bundle: nil), forSupplementaryViewOfKind: viewConfig.kind, withReuseIdentifier: viewConfig.viewIdentifier)
            
        default:
            print("")
        }
    }
    
    
    // MARK: - Sections
    public func add(section: DBCollectionViewSection, startPosition: Int? = nil) {
        let startPosition = startPosition ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition)
        
    }//Test Passed
    
    public func add(section: DBCollectionViewSection, after s: DBCollectionViewSection) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition+1)
    }//Test Passed
    
    public func add(section: DBCollectionViewSection, before s: DBCollectionViewSection) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: [section], startPosition: startPosition)
    }//Test Passed
    
    public func addAll(sections: [DBCollectionViewSection], startPosition: Int? = nil) {
        let startPosition = startPosition ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition)
    }
    
    public func addAll(sections: [DBCollectionViewSection], after s: DBCollectionViewSection) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition+1)
    }
    
    public func addAll(sections: [DBCollectionViewSection], before s: DBCollectionViewSection) {
        let startPosition = adapter.sections.indexByAddress(object: s) ?? self.adapter.sections.count
        addSections(sections: sections, startPosition: startPosition)
    }
    
    public func removeAll(from section: DBCollectionViewSection) {
        guard self.adapter.sections.indexByAddress(object: section) != nil else {
            return
        }
        //
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.deleteItems(section.getItems())
            section.items.removeAll()
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: false)
        }else{
            section.items.removeAll()
            reloadData()
        }
        
        
    }
    
    public func remove(section: DBCollectionViewSection) {
        guard self.adapter.sections.remove(object: section) != nil else {
            return
        }
        reloadData()
    }
    
    public func removeAll() {
        self.adapter.sections.removeAll()
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.deleteAllItems()
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            self.reloadData()
        }
        
    }
    
    // MARK:- Items
    public func add(item: DBModel, to section: DBCollectionViewSection, startPosition: Int? = nil) {
        let startPosition = startPosition ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition)
    }
    
    public func add(item: DBModel, to section: DBCollectionViewSection, after i: DBModel) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition+1)
    }
    
    public func add(item: DBModel, to section: DBCollectionViewSection, before i: DBModel) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: [item], to: section, startPosition: startPosition)
    }
    
    public func addAll(items: [DBModel], to section: DBCollectionViewSection, startPosition: Int? = nil) {
        let startPosition = startPosition ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition)
    }
    
    public func addAll(items: [DBModel], to section: DBCollectionViewSection, after i: DBModel) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition+1)
    }
    
    public func addAll(items: [DBModel], to section: DBCollectionViewSection, before i: DBModel) {
        let startPosition = section.items.index(object: i) ?? section.items.count
        addItems(items: items, to: section, startPosition: startPosition)
    }
    
    public func update(item: DBModel, animation: Bool = true) {
        guard let sectionAndIndex = self.adapter.findSection(by: item) else {
            return
        }
        guard let itemIndex = sectionAndIndex.section.items.index(object: item) else {
            return
        }
        if #available(iOS 13.0, *) {
            if self.adapter.snapshot?.itemIdentifiers.contains(item) ?? true{
                self.adapter.snapshot?.reloadItems([item])
                self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
            }
            
        }else{
            //sectionAndIndex.section.items.remove(at: itemIndex)
            //sectionAndIndex.section.items.insert(item, at: itemIndex)
            if self.getItems().contains(item) {
                animation ? self.reloadItems(at: [IndexPath(row: itemIndex, section: sectionAndIndex.index)]) : reloadData()
            }
        }
        
    }
    
    public func replace(previousItem: DBModel, with item: DBModel, animation: Bool = true) {
        guard let sectionAndIndex = self.adapter.findSection(by: previousItem) else {
            return
        }
        guard let itemIndex = sectionAndIndex.section.items.index(object: previousItem) else {
            return
        }
        sectionAndIndex.section.items.remove(at: itemIndex)
        sectionAndIndex.section.items.insert(item, at: itemIndex)
        animation ? self.reloadItems(at: [IndexPath(row: itemIndex, section: sectionAndIndex.index)]) : reloadData()
    }
    
    public func replaceAll(items: [DBModel], in section: DBCollectionViewSection) {
        if (!getSections().contains(section)) {
            section.items = items
            self.add(section: section)
        }else{
            removeAll(from: section)
            addAll(items: items, to: section)
        }
    }
    
    public func remove(item: DBModel, animation: Bool = true) {
        guard let sectionAndIndex = self.adapter.findSection(by: item) else {
            return
        }
        guard let itemIndex = sectionAndIndex.section.items.remove(object: item) else {
            return
        }
        animation ? reloadData() : self.deleteItems(at: [IndexPath(row: itemIndex, section: sectionAndIndex.index)])
    }
    
    public func showEmptyItem() {
        self.adapter.sections.removeAll()
        isGlobalEmptyCell = true
        self.isScrollEnabled = false
        let s = DBCollectionViewSection(items: [emptyModel])
        self.adapter.sections.insert(contentsOf: [s], at: 0)
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.deleteAllItems()
            self.adapter.snapshot?.appendSections([s])
            self.adapter.snapshot?.appendItems([emptyModel], toSection: s)
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            self.reloadData()
        }
    }
    
    public func showEmptyItem(in section: DBCollectionViewSection) {
        section.items.removeAll()
        section.items.append(section.emptyModel)
        if (!getSections().contains(section)) {
            self.addSections(sections: [section], startPosition: 0)
            return
        }
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.reloadSections([section])
        }else{
            self.reloadSections(IndexSet(integer: sectionIndex))
        }
    }
    
    public func showRetryItem() {
        isGlobalRetryCell = true
        self.adapter.sections.removeAll()
        self.reloadData()
        self.isScrollEnabled = false
        let s = DBCollectionViewSection()
        s.items.append(retryModel)
        self.adapter.sections.insert(contentsOf: [s], at: 0)
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.appendSections([s])
            self.adapter.snapshot?.appendItems(s.getItems(), toSection: s)
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            self.reloadData()
        }
    }
    
    public func showRetryItem(in section: DBCollectionViewSection) {
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            return
        }
        section.items.removeAll()
        section.items.append(retryModel)
        self.reloadSections(IndexSet(integer: sectionIndex))
    }
    
    public func removeEmptyItem() {
        isGlobalEmptyCell = false
        self.isScrollEnabled = true
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.deleteAllItems()
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            self.adapter.sections.removeAll()
            self.reloadData()
        }
    }
    
    public func removeRetryItem() {
        isGlobalRetryCell = false
        self.adapter.sections.removeAll()
        self.isScrollEnabled = true
    }
    //MARK:- Helpers
    
    private func  addSections(sections: [DBCollectionViewSection], startPosition: Int) {
        if (sections.isEmpty) {
            return
        }
        if (isGlobalRetryCell || isGlobalEmptyCell) {
            if isGlobalEmptyCell {self.removeEmptyItem()}
            if isGlobalRetryCell {self.removeRetryItem()}
            //self.reloadData()
        }
        
        var startPosition = startPosition
        if (startPosition < 0) {
            startPosition = 0
        }
        if (startPosition > self.adapter.sections.count) {
            startPosition = self.adapter.sections.count
        }
        self.adapter.sections.insert(contentsOf: sections, at: startPosition)
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.appendSections(sections)
            sections.forEach{self.adapter.snapshot?.appendItems($0.getItems(), toSection: $0)}
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            var indexSet = IndexSet()
            for i in startPosition...(startPosition + sections.count-1) {
                indexSet.insert(i)
            }
            if (self.numberOfSections == self.adapter.sections.count) {
                reloadData()
            }else{
                self.insertSections(indexSet)
            }
        }
        
        
    }
    
    public func findSection(by item: DBModel) -> DBCollectionViewSection? {
        return self.adapter.findSection(by: item)?.section
    }
    
    private func addItems(items: [DBModel], to section: DBCollectionViewSection, startPosition: Int) {
        
        if (items.isEmpty) {
            return
        }
        
        if (isGlobalRetryCell || isGlobalEmptyCell) {
            self.removeEmptyItem()
            self.removeRetryItem()
            self.reloadData()
        }
        
        guard let sectionIndex = self.adapter.sections.indexByAddress(object: section) else {
            section.items = section.items + items
            add(section: section)
            return
        }
        checkHelpingItems(section: section, sectionIndex: sectionIndex)
        var startPosition = startPosition
        if (startPosition < 0) {
            startPosition = 0
        }
        if (startPosition > section.items.count) {
            startPosition = section.items.count
        }
        section.items.insert(contentsOf: items, at: startPosition)
        if #available(iOS 13.0, *) {
            self.adapter.snapshot?.appendItems(items, toSection: section)
            self.adapter.dataSource?.apply(self.adapter.snapshot!,  animatingDifferences: true)
        }else{
            var indexPaths = [IndexPath]()
            for i in startPosition...(startPosition + items.count-1) {
                let indexPath = IndexPath(row: i, section: sectionIndex)
                indexPaths.append(indexPath)
            }
            if (self.numberOfItems(inSection: sectionIndex) == section.items.count) {
                reloadData()
            }else{
                self.insertItems(at: indexPaths)
            }
        }
        
    }
    
    private func checkHelpingItems(section: DBCollectionViewSection, sectionIndex: Int) {
        let retryIndex = section.items.remove(type: DBRetryModel.self)
        let emptyIndex = section.items.remove(type: DBEmptyModel.self)
        if (retryIndex != nil || emptyIndex != nil) {
            self.reloadSections(IndexSet(integer: sectionIndex))
        }
    }
    
    //MARK: - VisibleItems
    
    public func ifItemVisible(itemClass: AnyClass) -> Bool {
        var b = false
        let visibleCells = self.indexPathsForVisibleItems
        visibleCells.forEach { indexPath in
            let cell = adapter.getCell(indexPath: indexPath, collectionView: self)
            if (cell != nil) {
                let cellItem = adapter.getItem(indexPath: indexPath)
                if (String(describing: cellItem).contains(".\(itemClass):") && self.bounds.contains(cell!.bounds)) {
                    b = true
                }
            }
        }
        return b
    }
    
    
        
    
}
