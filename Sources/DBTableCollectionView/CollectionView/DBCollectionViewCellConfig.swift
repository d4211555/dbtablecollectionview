//
//  DBCollectionViewCellConfig.swift
//  DBTableCollectionView
//
//  Created by Dmitry Bogomazov on 30.03.2021.
//

import Foundation
import CoreGraphics
import UIKit

open class DBCollectionViewCellConfig {
    internal var type: AnyClass
    internal var cellIdentifier: String!
    internal weak var delegate: DBProtocol?
    internal var modelType: DBModel.Type!
    internal var registerType : RegisterCellType!
    internal var bundle : Bundle?
    internal var nibName: String?
    
    public init<T: DBModel>(type: DBCollectionViewCellParent<T>.Type, cellIdentifier: String, delegate: DBProtocol?, modelType: DBModel.Type, registerType : RegisterCellType = .none, bundle : Bundle? = nil, nibName: String?) {
        self.type = type
        self.cellIdentifier = cellIdentifier
        self.delegate = delegate
        self.modelType = modelType
        self.registerType = registerType
        self.bundle = bundle
        self.nibName = nibName
    }
    
    open func canEditing() -> Bool {
        return false
    }
    
    open func canMove() -> Bool {
        return false
    }
    
    open func deleteButtonTitle() -> String {
        return "Delete"
    }
    
    open func editingStyle() -> UITableViewCell.EditingStyle {
        return .delete
    }

    
    open func getSize(collectionView: UICollectionView, defautlItemSize: CGSize, section: DBCollectionViewSection) -> CGSize {
        print("section - \(section)")
        var h: CGFloat = 100
        let width = DBHelper.getCellWidth(collectionViewSize: collectionView.frame.size, section: section)
        if (section.height != nil) {
            h = section.height!
        }else if (section.heightRatio != nil){
            h = width * section.heightRatio!
        }
        return CGSize(width: width, height: h)
    }
    
}

public class DBHelper {
    public class func getCellWidth(collectionViewSize: CGSize, section: DBCollectionViewSection) -> CGFloat {
        
        if (section.items.count == section.itemCount) {
            section.widthRatio = 1.0
        }
        
        var inset = section.insets.left + section.insets.right
        if (section.widthRatio == 1 || section.horizontalTableView) {
            inset = inset / 2
        }else{
            inset = 0
        }
       
        let width = (collectionViewSize.width * section.widthRatio - section.itemSpacing) / CGFloat(section.itemCount) - inset
        return width
    }
    
    public class func getCellHeight(collectionViewSize: CGSize, section: DBCollectionViewSection) -> CGFloat {
        var h: CGFloat = 100.0
        let cellWidth = DBHelper.getCellWidth(collectionViewSize: collectionViewSize, section: section)
        if (section.height != nil) {
            h = section.height!
        }else if (section.heightRatio != nil){
            h = cellWidth * section.heightRatio! + section.insets.top + section.insets.bottom
        }
        return ceil(h) + section.lineSpacing + 2
    }
}

open class DBSupplementaryViewConfig {
    internal var type: AnyClass
    internal var viewIdentifier: String!
    internal weak var delegate: DBProtocol?
    internal var modelType: DBSupplementaryModel.Type!
    internal var registerType : RegisterCellType!
    internal var kind : String!
    
    public init<T: DBSupplementaryModel>(type: DBSupplementaryViewParent<T>.Type, viewIdentifier: String, delegate: DBProtocol?, modelType: DBSupplementaryModel.Type, kind: String, registerType : RegisterCellType = .none) {
        self.type = type
        self.viewIdentifier = viewIdentifier
        self.delegate = delegate
        self.modelType = modelType
        self.kind = kind
        self.registerType = registerType
    }
    
    open func automaticSize() -> Bool{
        return false
    }
    
    open func getSize(collectionViewSize: CGSize) -> CGSize {
            return CGSize(width: collectionViewSize.width, height: 50)
    }
    
}

